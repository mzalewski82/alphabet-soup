package com

package object mzalewski {

  def canWriteAMessage(message: String, soup: String): Boolean = {
    verifyInput(message, soup) && findLettersInSoup(message, soup)
  }

  private def verifyInput(message: String, soup: String): Boolean = {
    message != null && soup != null && message.length <= soup.length
  }

  private def findLettersInSoup(message: String, soup: String): Boolean = {
    var parsedMessage: Map[Char, Int] = getLetterMap(message)

    def findLetter(char: Char): Boolean = {
      val updatedCharCount = parsedMessage.getOrElse(char, 0) - 1
      updatedCharCount match {
        case -1 ⇒
        case 0  ⇒ parsedMessage = parsedMessage - char
        case _  ⇒ parsedMessage = parsedMessage.updated(char, updatedCharCount)
      }
      parsedMessage.nonEmpty
    }

    !soup.toCharArray.forall(char => findLetter(char))
  }

  private[mzalewski] def getLetterMap(string: String): Map[Char, Int] = string.groupBy(identity).mapValues(_.length)
}