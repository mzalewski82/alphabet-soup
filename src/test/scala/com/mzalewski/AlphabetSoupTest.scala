package com.mzalewski

import org.scalatest.WordSpec

class AlphabetSoupTest extends WordSpec {

  "AlphabetSoup" should {
    val message = "abca"

    "parse empty String" in {
      assert(getLetterMap("").isEmpty)
    }

    "parse short String" in {
      assert(getLetterMap("abcdaba") == Map('a' → 3, 'b' → 2, 'c' → 1, 'd' → 1))
    }

    "find exact match" in {
      assert(canWriteAMessage(message, message))
    }

    "find a match" in {
      assert(canWriteAMessage(message, "cbabcsacsjbc"))
    }

    "return true for empty message" in {
      assert(canWriteAMessage("", message))
    }

    "return false for empty soup" in {
      assert(!canWriteAMessage(message, ""))
    }

    "return false for null soup" in {
      assert(!canWriteAMessage(message, null))
    }

    "return false for null message" in {
      assert(!canWriteAMessage(null, ""))
    }

    "return false for incorrect soup" in {
      assert(!canWriteAMessage(message, "lalala"))
    }

    "return false for almost matching soup" in {
      assert(!canWriteAMessage(message, message.tail))
    }
  }
}
